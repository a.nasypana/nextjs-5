import { FC } from 'react';
import Image from 'next/image';

const Footer:FC = () => (
    <footer className='footer'>
        <div className='container'>
            <p className='footer__text'>
                <Image
                    src='/img/sign_logo.png'
                    alt=''
                    width={25}
                    height={25}
                />
                © 2020
                {' '}
                <strong>Lectrum LLC</strong>
                . All Rights Reserved.
            </p>

        </div>
    </footer>
);

export default Footer;
