'use client';

import { FC, ReactNode } from 'react';
import { usePathname, useRouter } from 'next/navigation';

interface IProps {
    children: ReactNode | ReactNode[];
    href: string,
}

export const TabButton:FC<IProps> = ({ children, href }) => {
    const router = useRouter();
    const pathname = usePathname();
    const styles = pathname === href ? 'tab-link tab-link--active' : 'tab-link';

    const handleClick = () => {
        router.push(href);
    };

    return (
        <button onClick={handleClick} className={styles} type='button'>
            {children}
        </button>
    );
};
