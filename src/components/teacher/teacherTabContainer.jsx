import { TabButton } from './tabButton';

export const TeacherTabContainer = () => (
   <section className='banner banner--pt'>
       <div className='container'>
           <div className='tabs' id='tab' role='tablist'>
               <TabButton
                   href='/teacher/about'>
                   About
               </TabButton>
               <TabButton
                   href='/teacher/courses'>
                   My courses
               </TabButton>
           </div>
       </div>
   </section>
);
