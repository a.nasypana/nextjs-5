'use client';

import { usePathname, useRouter } from 'next/navigation';

export const TabButton = ({ children, href }) => {
    const router = useRouter();
    const pathname = usePathname();
    const styles = pathname === href ? 'tab-link tab-link--active' : 'tab-link';

    const handleClick = () => {
        router.push(href);
    };

    return (
        <button onClick={handleClick} className={styles} type='button'>
            {children}
        </button>
    );
};
