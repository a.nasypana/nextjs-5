import Image from 'next/image';
import Link from 'next/link';

const Header = () => (
    <nav className='nav'>
        <div className='container'>
            <div className='nav__side-left'>
                <Link href='/' className='nav__logo'>
                    <Image
                        src='/img/logo.svg'
                        alt=''
                        width={135}
                        height={32}
                    />
                </Link>
            </div>
            <div className='nav__side-right'>
                <Link href='/' className='nav__btn'>Create New Course</Link>
                <Link href='/' className='nav__avatar avatar'>
                    <span className='avatar__name'> John Dou </span>
                    <Image
                        className='avatar__img'
                        src='/img/hd_dp.jpg'
                        width={36}
                        height={36}
                    />
                </Link>
                <Link href='/' className='nav__logout'>Log out</Link>
            </div>
        </div>
    </nav>
);

export default Header;
