const Footer = () => (
    <footer className='footer'>
        <div className='container'>
            <p className='footer__text'>
                <img src='img/sign_logo.png' alt='' />
                © 2020
                {' '}
                <strong>Lectrum LLC</strong>
                . All Rights Reserved.
            </p>

        </div>
    </footer>
);

export default Footer;
