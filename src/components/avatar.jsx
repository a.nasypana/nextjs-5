import Link from 'next/link';

const Avatar = () => (
        <div className='avatar-card'>
            <img src='./img/hd_dp.jpg' className='avatar-card__img' alt='' />
            <p className='avatar-card__name'>Joginder Singh</p>
            <p className='avatar-card__descr'>Web Developer, Designer, and Teacher</p>
            <ul className='avatar-card__stats'>
                <li>615K Students</li>
                <li> 12 Courses</li>
            </ul>
            <Link href='/teacher/about'>Go To Profile</Link>
        </div>
    );

export default Avatar;
