import { FC } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { timeAgo } from '../utils/time';
import { ICourse } from '../types';

interface IProps {
    course: ICourse,
}

const Course:FC<IProps> = (props) => {
    const { course } = props;

    const requirementsJSX = course?.info?.requirements
        && Array.isArray(course.info.requirements)
        && course.info.requirements.map((r) => (<li key={r} className='list__item'>{r}</li>));

    const descriptionsJSX = course?.info?.descriptions
        && Array.isArray(course.info.descriptions)
        && course.info.descriptions.map((d) => (<li key={d} className='list__item'>{d}</li>));

    const benefitsJSX = course?.info?.benefits
        && Array.isArray(course.info.benefits)
        && course.info.benefits.map((b) => (<li key={b} className='list__item list__item--bold'>{b}</li>));

    return (
        <main className='courses-detail-layout course-detail'>
            <header className='header'>
                <div className='container'>
                    <div className='course-detail__header'>
                        <div className='course-detail__header-left'>
                            <div className='course-card'>
                                <Link
                                    href={`/courses/${course?.hash || ''}`}
                                    className='course-card__header course-card__header--bigger'>
                                   {/* <Image
                                        src={`/${course?.poster || ''}`}
                                        layout='responsive'
                                        width={480}
                                        height={270}
                                        alt=''
                                    />*/}
                                    <img src={course?.poster || ''} alt=''/>
                                    <div className='course-card__header-overlay'>
                                        {course?.badge && <div className='course-card__label-bestseller'>Bestseller</div>}
                                        <div className='course-card__label-reviews'>{course?.views || ''}</div>
                                        <div className='course-card__label-timer'>{course?.duration || ''} hours</div>
                                    </div>

                                </Link>
                            </div>
                        </div>
                        <div className='course-detail__header-right'>
                            <h2 className='course-detail__title'>{course?.description || ''}</h2>
                            <p className='course-detail__summary'>
                                {course?.info?.descriptionSummary}
                            </p>
                            <div className='course-detail__rating-wrap'>
                                <div className='course-detail__rating'>
                                    {course?.rating || ''}
                                </div>
                                ({course?.views || ''} ratings)
                            </div>
                            <div className='course-detail__stats'>
                                114,521 students enrolled
                            </div>
                            <div className='course-detail__lang'>
                                English
                            </div>
                            <div className='course-detail__upd'>
                                Last updated {timeAgo(course?.created || '')}
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section className='banner banner--pd '>
                <div className='container'>
                    <div className='course-detail__info'>
                        <div className='course-detail__author'>
                            <div className='avatar'>
                                <Image
                                    className='avatar__img avatar__img--bigger'
                                    src='/img/courses/hd_dp.jpg'
                                    layout='responsive'
                                    width={80}
                                    height={70}
                                    alt=''
                                />
                                <Link href='/teacher/about' className='avatar__name'>
                                    {course?.createdBy || ''}
                                </Link>
                            </div>
                        </div>
                        <div className='course-detail__react'>
                            <div className='course-detail__react-icon course-detail__react-icon--like'>
                                <Image
                                    className='course-detail__react-img'
                                    src='/img/like.svg'
                                    layout='responsive'
                                    width={16}
                                    height={16}
                                    alt=''
                                />
                                <span>200</span>
                            </div>
                            <div className='course-detail__react-icon course-detail__react-icon--dislike'>
                                <Image
                                    className='course-detail__react-img'
                                    src='/img/dislike.svg'
                                    layout='responsive'
                                    width={16}
                                    height={16}
                                    alt=''
                                />
                                <span>50</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className='description-layout'>
                <div className='container'>
                    <h3 className='subtitle-h3'>Requirements</h3>
                    <ul className='list list--ul'>
                        {requirementsJSX}
                    </ul>

                    <h3 className='subtitle-h3'>Requirements</h3>
                    <ul className='list list--ul'>
                        {descriptionsJSX}
                    </ul>
                    <p className='par'>
                        When you're learning to program you often have to sacrifice learning the exciting and current
                        technologies in favor of the "beginner friendly" classes. With this course, you get the best of
                        both
                        worlds. This is a course designed for the complete beginner, yet it covers some of the most
                        exciting
                        and relevant topics in the industry.
                    </p>
                    <p className='par'>
                        Throughout the course we cover tons of tools and technologies including:
                    </p>
                    <ul className='list list--ul'>
                        {benefitsJSX}
                    </ul>
                </div>
            </section>
        </main>
    );
};

export default Course;
