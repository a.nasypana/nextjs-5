import Link from 'next/link';
import Image from 'next/image';
import { timeAgo } from '../utils/time';

const CourseCard = (props) => {
    const { course } = props;

    return (
        <div className='course-card'>
            <Link href={`/courses/${course?.hash || ''}`} className='course-card__header'>
                <Image
                    src={`/${course?.poster || ''}`}
                    layout='responsive'
                    width={480}
                    height={270}
                    alt=''
                />
                <div className='course-card__header-overlay'>
                    {course?.badge && <div className='course-card__label-bestseller'>Bestseller</div>}
                    <div className='course-card__label-reviews'>{course?.rating}</div>
                    <div className='course-card__label-timer'>{`${course?.duration} hours`}</div>
                </div>
            </Link>

            <div className='course-card__footer'>
                <ul className='course-card__stats'>
                    <li>{course?.views || ''} views</li>
                    <li>{timeAgo(course?.updated || '')}</li>
                </ul>
                <Link href={`/courses/${course?.hash || ''}`} className='course-card__title'>
                    {course?.description || ''}
                </Link>
                <Link href='/' className='course-card__cate'>{course?.technologies || ''}</Link>
                <div className='course-card__info'>
                    <p className='course-card__author'>By <Link href='/teacher/about'>{course?.createdBy || ''}</Link></p>
                    <div className='course-card__price'>{`$${course?.price || ''}`}</div>
                </div>
            </div>
        </div>
    );
};

export default CourseCard;
