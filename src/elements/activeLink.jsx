'use client';

import { usePathname, useRouter } from 'next/navigation';

export const ActiveLink = ({
                               children, href, nameClass = '', activeClass = '',
}) => {

    const router = useRouter();
    const pathname = usePathname();
    const styles = pathname === href ? `${nameClass + activeClass}` : nameClass;

    const handleClick = (event) => {
        event.preventDefault();
        router.push(href);
    };

    return (
        <a href={href} onClick={handleClick} className={styles}>
            {children}
        </a>
    );
};
