'use client';

import { FC, ReactNode } from 'react';
import { usePathname, useRouter } from 'next/navigation';

interface IProps {
    children: ReactNode | ReactNode[];
    href: string,
    nameClass?: string,
    activeClass?: string,
}

export const ActiveLink:FC<IProps> = ({
                               children, href, nameClass = '', activeClass = '',
}) => {

    const router = useRouter();
    const pathname = usePathname();
    const styles = pathname === href ? `${nameClass + activeClass}` : nameClass;

    const handleClick = (event) => {
        event.preventDefault();
        router.push(href);
    };

    return (
        <a href={href} onClick={handleClick} className={styles}>
            {children}
        </a>
    );
};
