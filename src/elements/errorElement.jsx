import Image from 'next/image';
import Link from 'next/link';

const ErrorElement = ({ statusCode, title }) => (
    <div className='error'>
        <div className='error__container'>
            <div className='error_header_container'>
                <Link href='/'>
                    <Image
                        src='/img/ct_logo.svg'
                        alt='logo'
                        layout='fixed'
                        width={300}
                        height={71}
                    />
                </Link>

                <h1 className='error__title'>{ statusCode }</h1>
            </div>

            <div className='navigation'>
                <Link href='/'>Go To Homepage</Link>
            </div>
            <div className='divider' />
            <div className='error__text-container'>
                <h4 className='error_subtitle'>{ title }</h4>
            </div>
        </div>
    </div>

);

export default ErrorElement;
