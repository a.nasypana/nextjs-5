export interface IInfoCourse {
    requirements: string[];
    descriptions: string[];
    benefits: string[];
    descriptionSummary: string;
}

export interface ICourse {
    badge: boolean;
    poster: string;
    duration: number;
    description: string;
    technologies: string;
    price: number;
    info: IInfoCourse;
    hash: string;
    rating: number;
    views: number;
    createdBy: string;
    created: string;
}
