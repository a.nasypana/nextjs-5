import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';

export const timeAgo = (str) => {
    if (!str) {
        return '';
    }

    TimeAgo.addLocale(en);

    const timeAgo = new TimeAgo('en-US');

    return timeAgo.format(new Date(str));
};
