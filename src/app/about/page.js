import Link from 'next/link';

const AboutPage = () => (
        <>
            <Link href='/'>Home</Link>
            <h1>About Page</h1>
        </>
    );

export default AboutPage;
