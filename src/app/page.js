import Avatar from '@/components/avatar';
import CourseCard from '@/components/courseCard';
import { mockedCoursesData } from '../mock/courses';

const Home = () => {
    const coursesJSX = mockedCoursesData.map((c) => (<CourseCard key={c.hash} course={{ ...c }} />));

    return (
        <main className='courses-layout container'>
            <section className='courses-layout__side-left'>
                <div className='courses-layout__container'>
                    { coursesJSX }
                </div>
            </section>
            <aside className='courses-layout__side-right'>
                <Avatar />
            </aside>
        </main>
    );
};

export default Home;
