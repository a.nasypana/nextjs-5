'use client';
import { NextPage } from 'next';

import ErrorElement from '@/elements/errorElement';
import { IErrorResponse } from '../types';

interface IPtrops{
    error: IErrorResponse;
}

const Error:NextPage<IPtrops> = ({ error }) => (
        <ErrorElement
            statusCode={error?.status}
            title={error?.message} />
);

export default Error;
