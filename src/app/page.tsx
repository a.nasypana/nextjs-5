import axios from 'axios';
import { notFound } from 'next/navigation';
import { NextPage } from 'next';
import Avatar from '@/components/avatar';
import CourseCard from '@/components/courseCard';
import { mockedCoursesData } from '../mock/courses';
import { ICourse } from '../types';

const getCourses = async (): Promise<ICourse[]> => {
    const res = await fetch(`${process.env.API_URL}/courses`, {
        cache: "no-cache"
    });
    const data = await res.json();
    const courses = data?.data as ICourse[];
    return courses;
};

const HomePage = async () => {
   /* const coursesJSX = mockedCoursesData
        .map((c: ICourse) => (<CourseCard key={c.hash} course={{ ...c }} />));*/
    const data = await getCourses();

    const coursesJSX = data.map((c: ICourse) => (<CourseCard key={c.hash} course={{ ...c }} />));

    return (
        <main className='courses-layout container'>
            <section className='courses-layout__side-left'>
                <div className='courses-layout__container'>
                    { coursesJSX }
                </div>
            </section>
            <aside className='courses-layout__side-right'>
                <Avatar />
            </aside>
        </main>
    );
};

export default HomePage;
