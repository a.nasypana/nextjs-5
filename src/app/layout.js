import './globals.css';
import { Inter } from 'next/font/google';

import Footer from '@/components/footer';
import Header from '@/components/header';

const inter = Inter({ subsets: ['latin'] });

export const metadata = {
    title: 'Welcome to the Next.js Course!',
    description: 'Lectrum Next.js course',
};

const RootLayout = ({ children }) => (
    <html lang='en'>
        <body className={`${inter.className} form-back`}>
            <Header />
                { children }
            <Footer />
        </body>
    </html>
);

export default RootLayout;
