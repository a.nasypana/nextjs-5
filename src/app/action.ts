'use server';

import { cookies } from 'next/headers';

export const getAllCookies = async () => {

    return await cookies().getAll();
};

export const getCookie = async (key) => {
    const cookie = await cookies().get(key);
    return cookie?.value;
}

export const setCookie = async (key, value) => {
    return await cookies().set(key, value);
}
