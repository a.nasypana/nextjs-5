import Course from '@/components/course';
import { mockedCoursesData } from '../../../mock/courses';

const CourseDetailPage = ({ params }) => <Course course={mockedCoursesData.find((c) => c.hash === params.hash)} />;

export const generateStaticParams = () => mockedCoursesData.map((c) => ({ hash: c.hash }));

export default CourseDetailPage;
