import { NextPage } from 'next';
import { notFound } from 'next/navigation';
import Course from '@/components/course';
import { mockedCoursesData } from '../../../mock/courses';
import { ICourse } from '../../../types';

interface IParams {
    hash: string;
}

interface IProps {
    params: IParams;
}

const getCourse = async (id: string): Promise<ICourse | null> => {
    await fetch(`${process.env.API_URL}/courses/${id}/views`, {
        cache: 'no-cache',
        method: 'PUT'
    });

    const res = await fetch(`${process.env.API_URL}/courses/${id}`, {
        cache: 'no-cache',
    });

    if (res.status !== 200) {
        return null;
    }

    const data = await res.json();
    const course = data?.data as ICourse;

    return course;
}

const CourseDetailPage = async ({ params }) => {
    const course = await getCourse(params.hash);

    if (!course) {
        notFound();
    }

    return <Course course={{ ...course }} />;
};

export const generateStaticParams = ():IParams[] => mockedCoursesData.map((c) => ({ hash: c.hash }));

export default CourseDetailPage;
