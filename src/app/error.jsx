'use client';

import ErrorElement from '@/elements/errorElement';

const Error = ({ error }) => (
        <ErrorElement
            statusCode={error?.status}
            title={error?.message} />
);

export default Error;
