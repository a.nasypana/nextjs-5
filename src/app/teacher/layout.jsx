import { TeacherHeader, TeacherTabContainer } from '@/components/teacher';

const Layout = ({ children }) => (
    <main>
        <TeacherHeader />
        <TeacherTabContainer />
        <section className='description-layout'>
            <div className='container'>
                <div className='tab-content'>
                    { children }
                </div>
            </div>
        </section>
    </main>

);

export default Layout;
