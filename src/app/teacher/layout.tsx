import { ReactNode } from 'react';
import { NextPage } from 'next';
import { TeacherHeader, TeacherTabContainer } from '@/components/teacher';

interface IProps {
    children: ReactNode | ReactNode[];
}

const TeacherLayout:NextPage<IProps> = ({ children }) => (
    <main>
        <TeacherHeader />
        <TeacherTabContainer />
        <section className='description-layout'>
            <div className='container'>
                <div className='tab-content'>
                    { children }
                </div>
            </div>
        </section>
    </main>
);

export default TeacherLayout;
