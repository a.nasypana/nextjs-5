import { NextPage } from 'next';
import { notFound } from 'next/navigation';
import CourseCard from '@/components/courseCard';
import { mockedCoursesData } from '../../../mock/courses';
import { ICourse } from '../../../types';

const getCoursesByTeacher = async (): Promise<ICourse[] | null> => {
    const res = await fetch(`${process.env.API_URL}/courses`, {
        cache: "no-cache"
    });

    const data = await res.json();
    const courses = data?.data as ICourse[];
    return courses.filter(c => c.createdBy === 'John Doe');
}

const TeacherCourses = async (): Promise<JSX.Element> => {
    const data = await getCoursesByTeacher();
    
    if (!data) {
        notFound();
    }
    const coursesJSX = data.map((c: ICourse) => (<CourseCard key={c.hash} course={{ ...c }} />));

    return (
        <div className='tab-pane show'>
            <h3 className='subtitle-h3'>
                My courses ({coursesJSX.length})
            </h3>
            <div className='courses-layout__container'>
                { coursesJSX }
            </div>
        </div>
    );
};

export default TeacherCourses;
