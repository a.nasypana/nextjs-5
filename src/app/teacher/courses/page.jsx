import CourseCard from '@/components/courseCard';
import { mockedCoursesData } from '../../../mock/courses';

const TeacherCourses = () => {
    const coursesJSX = mockedCoursesData.filter((c) => c.createdBy === 'John Doe')
        .map((c) => (<CourseCard key={c.hash} course={{ ...c }} />));

    return (
        <div className='tab-pane show'>
            <h3 className='subtitle-h3'>
                My courses ({coursesJSX.length})
            </h3>
            <div className='courses-layout__container'>
                { coursesJSX }
            </div>
        </div>
    );
};

export default TeacherCourses;
