import ErrorElement from '@/elements/errorElement';

const NotFound = () => (
    <ErrorElement
        statusCode='404'
        title='This page could not be found.' />
    );

export default NotFound;
